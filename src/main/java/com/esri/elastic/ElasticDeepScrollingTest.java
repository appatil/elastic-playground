package com.esri.elastic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang3.ObjectUtils;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.index.IndexRequest;
import org.json.simple.JSONObject;

/**
 * Proof of concept for elastic search deep scrolling. Adds 100000 docs and get
 * 100000 results in a search query. This is to overcome the limitation of
 * elastic search which returns 10000 documents in search result. Adds 100000
 * docs with identical "message":"adding data" content Search on the index with
 * the param "message":"adding data" this shall return 100000 docs retrieved
 * through deep scrolling
 * 
 * @author anik7857
 */
public class ElasticDeepScrollingTest {
	
	 static{
		  try{
		  PropertiesConfiguration config = new PropertiesConfiguration();
	      config.load("deep_scroll.properties");
	      NUM_OF_DOCS = (int) config.getProperty("max.docs");
		  }catch(Exception e){
			  NUM_OF_DOCS = 1000000;
		  }
	  }
	 
	public static final int MAX_NUM_THREADS = 8;
	 
	private static ElasticSearchManager manager = ElasticSearchManager.getInstance();

	private static final String INDEX_NAME = "test_index";

	private static final String INDEX_MAPPING = "test_index_mappings";

	private static int numThreads       = MAX_NUM_THREADS;
	  
	private static int NUM_OF_DOCS      = 1000000;

	private static BulkProcessor bulkProcessor = null;
	  static {
	    numThreads = Math.min(Runtime.getRuntime().availableProcessors(), MAX_NUM_THREADS);
	  }
	  
	  public static void main(String args[]){
		  System.out.println(" Starting the Deep Scrolling Test ");
		if (ObjectUtils.allNotNull(manager)) {
			manager.initializeElasticNode();
			manager.intializeBulkProcessor();
			bulkProcessor = manager.getBulkProcessor();
			ElasticDeepScrollingTest scrollTest = new ElasticDeepScrollingTest();
			scrollTest.testScrolling();
			manager.closeBulkProcessor();
			manager.destroyElasticNode();
		}
		  System.out.println(" Executed the Deep Scrolling Test ");
	  }

	public void testScrolling() {
		try {
			if (ObjectUtils.allNotNull(manager)) {
				int docPerThread = NUM_OF_DOCS/numThreads;
				manager.createIndex(INDEX_NAME);
				manager.createMapping(INDEX_NAME, INDEX_MAPPING);

				// add 100000 docs with identical "message":"adding data" content
				ExecutorService threadPool = Executors.newFixedThreadPool(numThreads);
				CompletionService<Void> pool = new ExecutorCompletionService<Void>(threadPool);
				try {
					int submits = 0;
					for (int i = 1; i <= numThreads; i++) {
						pool.submit(new AddDocCallable(docPerThread));
						submits++;
					}
					for (int i = 0; i < submits; i++) {
						pool.take().get();
					}
				    } finally {
					threadPool.shutdownNow();
					threadPool = null;
				    }
				System.out.println(" Number of documents added to index " + NUM_OF_DOCS);
				Thread.sleep(10000);
                manager.flushBulkProcessor();
				// search on the index with the param "message":"adding data" this shall return 100000 docs retrieved through deep scrolling
				Map<String, Object> queryParams = new HashMap<String, Object>();
				queryParams.put("message", "adding data");
				List<String> results = manager.searchLargeResults(INDEX_NAME, INDEX_NAME, queryParams);
				System.out.println(" Number of docs returned in search " + results.size());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	  private class AddDocCallable implements Callable<Void> {
		  
		  private int docs;

		  public AddDocCallable(int docs){
			  this.docs = docs;
		  }
		public Void call() throws Exception {
			for (int i = 0; i < docs; i++) {
				UUID uuid = UUID.randomUUID();
			    String _id = uuid.toString();
				Map<String, Object> addDoc = new HashMap<String, Object>();
				addDoc.put("user", _id);
				addDoc.put("message", "adding data");

				IndexRequest indexReq  = new IndexRequest(INDEX_NAME);
				indexReq.id(_id);
				indexReq.type(INDEX_NAME);
				indexReq.source(addDoc);
				bulkProcessor.add(indexReq);
			}
			return null;
		    }
		  }
}
