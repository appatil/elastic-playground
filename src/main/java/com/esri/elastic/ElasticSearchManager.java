package com.esri.elastic;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.logging.Loggers;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.unit.ByteSizeUnit;
import org.elasticsearch.common.unit.ByteSizeValue;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.common.xcontent.XContentParser;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.esotericsoftware.yamlbeans.YamlWriter;

/**
 * Manages an elastic 5.0 instance. Provides functionalities
 * to initialize an elastic node, start a elastic node, stop a elastic node,
 * clean up a node, connect to node, disconnect from a node, add index and
 * mappings, CRUD operation on index.
 * 
 * @author anik7857
 */
public class ElasticSearchManager {
  private static String RUNTIME_DIR = null;
  private static String HTTP_PORT = null;
  private static String TRANSPORT_PORT = null;
  private TransportClient client = null;
  private static String CLUSTER_NAME = null;
  private BulkProcessor bulkProcessor = null;

  private Logger LOGGER = Loggers.getLogger(ElasticSearchManager.class);
  
  static{
	  try{
	  PropertiesConfiguration config = new PropertiesConfiguration();
      config.load("elastic-search.properties");
      RUNTIME_DIR = (String) config.getProperty("runtime.path");
      HTTP_PORT = (String) config.getProperty("http.port");
      TRANSPORT_PORT = (String) config.getProperty("transport.port");
      CLUSTER_NAME = (String) config.getProperty("cluster.name");
	  }catch(Exception e){
		 //ignore
	  }
  }

  public static ElasticSearchManager getInstance() {
    return ElasticSearchManagerHolder.INSTANCE;
  }

  private static class ElasticSearchManagerHolder {
    private static final ElasticSearchManager INSTANCE = new ElasticSearchManager();
  }

  /**
   * Initializes an elastic node with the configuration provided in
   * elastic-search.properties. The parameters used from the properties file are
   * runtime.path, http.port , transport.port and cluster.name. The elastic yml
   * file is updated, node is started and client is connected to the node.
   */
  public void initializeElasticNode() {
    configureElasticNode();
    startElasticNode();
    connect();
  }

  /**
   * Starts an elastic instance on local machine as a background process.
   */
  public void startElasticNode() {
    try {
      if (!isNodeRunning()) {
        String command = "elasticsearch";
        if (System.getProperty("os.name").toLowerCase().contains("linux"))
          command = "./elasticsearch";
        String pidFile = RUNTIME_DIR + File.separator + "pids" + File.separator + "elastic.pid";
        command = command + " -d -p \"" + pidFile + "\"";
        ElasticUtil.executeElasticCommand(RUNTIME_DIR, command, false);
        Thread.sleep(30000);
      }
    } catch (Exception e) {
      LOGGER.debug(e);
    } finally {
    }
  }

  /**
   * Stops an elastic instance on local machine.
   */
  public void stopElasticNode() {
    try {
      if (isNodeRunning()) {
        ElasticUtil.killElasticsearchFromPIDFile(RUNTIME_DIR);
        Thread.sleep(30000);
      }
    } catch (Exception e) {
      LOGGER.debug(e);
    } finally {
    }
  }

  /**
   * Checks if elastic instance is running on the local machine.
   * 
   * @return boolean true or false.
   */
  public boolean isNodeRunning() {
    boolean isRunning = false;
    try {
      if (!ObjectUtils.allNotNull(client)) {
        connect();
      }
      client.admin().cluster().prepareClusterStats().execute();
      isRunning = true;
    } catch (Exception e) {
      LOGGER.debug(e);
    }
    return isRunning;
  }

  /**
   * Disconnects a client connection to elastic instance on the local machine.
   *
   */
  public void disconnect() {
    try {
      if (ObjectUtils.allNotNull(client)) {
        client.close();
        client = null;
      }
    } catch (Exception e) {
      LOGGER.debug(e);
    } finally {
    }
  }

  /**
   * Return a client connection object to elastic instance on the local machine.
   * 
   * @return Transport client object reference
   */
  public TransportClient getClient() {
    return client;
  }

  /**
   * Shuts down a node, disconnect the client and cleans up an elastic node
   *
   */
  public void destroyElasticNode() {
    stopElasticNode();
    disconnect();
    cleanup();
  }

  /**
   * Cleans up elastic data. Deletes the elastic data directory and the pid file
   * for elastic process.
   *
   */
  private void cleanup() {
    try {
      if (StringUtils.isNotBlank(RUNTIME_DIR)) {
        String dataPath = RUNTIME_DIR + File.separator + "elasticsearch" + File.separator + "esdata";
        String pidPath = RUNTIME_DIR + File.separator + "pids" + File.separator + "elastic.pid";
        File file = new File(dataPath);
        if (file.isDirectory()) {
          FileUtils.deleteDirectory(file);
        }
        file = new File(pidPath);
        if (file.exists()) {
          FileUtils.deleteQuietly(file);
        }
      }
    } catch (Exception e) {
      LOGGER.debug(e);
    } finally {
    }
  }

  /**
   * Configures an elastic instance. Reads the params from the elastic
   * search.properties file, updates the elasticsearch.yml file.
   * 
   *
   */
  private void configureElasticNode() {
    try {
      

      String configFilePath = RUNTIME_DIR + File.separator + "elasticsearch" + File.separator + "config"
          + File.separator + "elasticsearch.yml";

      Map<String, String> configParams = new HashMap<String, String>();
      configParams.put("cluster.name", CLUSTER_NAME);
      configParams.put("path.data", RUNTIME_DIR + File.separator + "elasticsearch" + File.separator + "esdata");
      configParams.put("path.logs", RUNTIME_DIR + File.separator + "elasticsearch" + File.separator + "eslogs");
      configParams.put("network.host", "_local_");
      configParams.put("http.port", HTTP_PORT);
      configParams.put("transport.tcp.port", TRANSPORT_PORT);
      configParams.put("node.max_local_storage_nodes", "1");

      YamlWriter writer = new YamlWriter(new FileWriter(configFilePath));
      writer.write(configParams);
      writer.close();

    } catch (Exception e) {
      LOGGER.debug(e);
    } finally {
    }
  }

  /**
   * Creates an index in elastic search. If index already exist it shall delete
   * the index and recreate one.
   * 
   * @param indexName
   *          the name of index to be deleted.
   */
  public void createIndex(String indexName) {
    try {
      if (!ObjectUtils.allNotNull(client)) {
        connect();
      }
      if (doesIndexExist(indexName)) {
        DeleteIndexRequestBuilder deleteIndexRequest = client.admin().indices().prepareDelete(indexName);
        deleteIndexRequest.execute().actionGet();
      }
      CreateIndexRequestBuilder createIndexRequest = client.admin().indices().prepareCreate(indexName);
      createIndexRequest.execute().actionGet();
    } catch (Exception e) {
      LOGGER.debug(e);
    }
  }

  /**
   * Deletes an index in elastic search.
   * 
   * @param indexName
   *          the name of index to be deleted.
   */
  public void deleteIndex(String indexName) {
    try {
      if (!ObjectUtils.allNotNull(client)) {
        connect();
      }
      if (doesIndexExist(indexName)) {
        DeleteIndexRequestBuilder deleteIndexRequestBuilder = client.admin().indices().prepareDelete(indexName);
        deleteIndexRequestBuilder.execute().actionGet();
      }
    } catch (Exception e) {
      LOGGER.debug(e);
    }
  }

  /**
   * Checks if the index already exist in elastic search.
   * 
   * @param indexName
   *          the index name to be checked.
   * @return boolean true or false.
   */
  public boolean doesIndexExist(String indexName) {
    try {
      final IndicesExistsResponse indexExist = client.admin().indices().prepareExists(indexName).execute().actionGet();
      if (indexExist.isExists()) {
        return true;
      }
    } catch (Exception e) {
      LOGGER.debug(e);
    }
    return false;
  }

  /**
   * Creates mappings for index. The index need to already added to the elastic
   * server before creating the mappings
   * 
   * @param indexName
   *          the name of the index
   * @param indexMappings
   *          the name of file with the mappings foe the index.
   *
   */
  public void createMapping(String indexName, String indexMappings) {
    try {
      if (ObjectUtils.allNotNull(client)) {
        JSONParser jsonParser = new JSONParser();
        ClassLoader classLoader = getClass().getClassLoader();
        Object obj = jsonParser.parse(new FileReader(classLoader.getResource(indexMappings).getFile()));
        JSONObject jsonObject = (JSONObject) obj;

        XContentParser parser = XContentFactory.xContent(XContentType.JSON)
            .createParser(jsonObject.toString().getBytes());
        parser.close();
        XContentBuilder mappings = jsonBuilder().copyCurrentStructure(parser);

        client.admin().indices().preparePutMapping(indexName).setType(indexName).setSource(mappings).execute()
            .actionGet();
      }
    } catch (Exception e) {
      LOGGER.debug(e);
    }
  }

  /**
   * Connects to an already running instance of elastic search on the local
   * machine.
   */
  @SuppressWarnings("resource")
  public void connect() {
    try {
      if(!ObjectUtils.allNotNull(client)){
      Settings settings = Settings.builder().put("cluster.name", CLUSTER_NAME).build();
      client = new PreBuiltTransportClient(settings).addTransportAddress(
          new InetSocketTransportAddress(InetAddress.getByName("localhost"), Integer.parseInt(TRANSPORT_PORT)));
      }
    } catch (Exception e) {
      LOGGER.debug(e);
    }
  }

  /**
   * Adds a document to index.
   * 
   * @param indexName
   *          the name of index to which document needs to be added.
   * @param indexType
   *          the type of index for the document.
   * @param documentName
   *          the name of the document. This is the unique id for the document.
   * @param docContent
   *          the content of document in a json string format.
   * @return true or false
   */
  public boolean addDocument(String indexName, String indexType, String documentName, String docContent) {
    boolean docAdded = false;
    try {
      if (ObjectUtils.allNotNull(client)) {
        IndexResponse indexResponse = client.prepareIndex(indexName, indexType, documentName).setSource(docContent)
            .execute().actionGet();
        if (ObjectUtils.allNotNull(indexResponse) && indexResponse.status() == RestStatus.CREATED) {
          docAdded = true;
        }
      }
    } catch (Exception e) {
      LOGGER.debug(e);
    }
    return docAdded;
  }

  /**
   * Deletes a document form the index using the document id.
   * 
   * @param indexName
   *          the name of index from which document needs to be deleted.
   * @param indexType
   *          the type of index for the document.
   * @param id
   *          This is the unique id for the document which is the document name.
   * @return true or false
   */
  public boolean deleteDocument(String indexName, String indexType, String id) {
    boolean docDeleted = false;
    try {
      if (ObjectUtils.allNotNull(client)) {
        DeleteResponse deleteResponse = client.prepareDelete(indexName, indexType, id).get();
        if (ObjectUtils.allNotNull(deleteResponse) && deleteResponse.status() == RestStatus.OK) {
          docDeleted = true;
        }
      }
    } catch (Exception e) {
      LOGGER.debug(e);
    }
    return docDeleted;
  }

  /**
   * Updates an existing document in the index.
   * 
   * @param indexName
   *          the name of index from which document needs to be updated.
   * @param indexType
   *          indexType the type of index for the document.
   * @param documentName
   *          This is the unique id for the document which is the document name.
   * @param docContent
   *          the updated content of document in a json string format.
   * @return true or false
   */
  public boolean updateDocument(String indexName, String indexType, String documentName, String docContent) {
    boolean updatedDoc = false;
    try {
      UpdateRequest updateRequest = new UpdateRequest();
      updateRequest.index(indexName);
      updateRequest.type(indexType);
      updateRequest.id(documentName);
      updateRequest.doc(docContent);
      UpdateResponse updateResponse = client.update(updateRequest).get();

      if (ObjectUtils.allNotNull(updateResponse) && updateResponse.status() == RestStatus.OK) {
        updatedDoc = true;
      }
    } catch (Exception e) {
      LOGGER.debug(e);
    }
    return updatedDoc;
  }

  /**
   * Return a document from index. The document is fetched using the document
   * name.
   * 
   * @param indexName
   *          the name of index from which document needs to be fetched.
   * @param indexType
   *          the type of index for the document.
   * @param documentName
   *          This is the unique id for the document which is the document name.
   * @return document String representation of the document.
   */
  public String getDocument(String indexName, String indexType, String documentName) {
    String document = null;
    try {
      if (ObjectUtils.allNotNull(client)) {
        GetResponse getResponse = client.prepareGet(indexName, indexType, documentName).get();
        if (ObjectUtils.allNotNull(getResponse) && getResponse.isExists()) {
          Map<String, Object> docData = getResponse.getSource();
          JSONObject json = new JSONObject(docData);
          document = json.toString();
        }
      }
    } catch (Exception e) {
      LOGGER.debug(e);
    } finally {
    }
    return document;
  }

  /**
   * Searches a list of documents matching the search criteria
   * 
   * @param indexName
   *          the name of index from which document needs to be searched.
   * @param indexType
   *          the type of index for the document.
   * @param queryParams
   *          map with key value pair search criteria.
   * @return Lists of document matching search criteria.
   */

  public List<String> searchDocument(String indexName, String indexType, Map<String, Object> queryParams) {
    List<String> results = new ArrayList<>();
    try {
      if (ObjectUtils.allNotNull(client)) {
        QueryBuilder searchQuery = termQueryBuilder(queryParams);
        SearchResponse response = client.prepareSearch(indexName).setTypes(indexType)
            .setSearchType(SearchType.DFS_QUERY_THEN_FETCH).setQuery(searchQuery).setFrom(0).setSize(60)
            .setExplain(true).execute().actionGet();

        if (ObjectUtils.allNotNull(response) && response.status() == RestStatus.OK) {
          response.getHits().forEach(hit -> {
            Map<String, Object> contentData = hit.getSource();
            JSONObject json = new JSONObject(contentData);
            String contentString = json.toString();
            results.add(contentString);
          });
        }
      }
    } catch (Exception e) {
      LOGGER.debug(e);
    }
    return results;
  }

  /**
   * This method adds a bulk of documents to elastic search. This method can be
   * used to add documents to elastic search when a number of documents need to
   * be added at once. This is cleaner,faster and efficient way to add a number
   * of documents.
   * 
   * @param indexName
   *          This is the name of the index
   * @param indexType
   *          This is the type of index
   * @param docs
   *          This is Map containing the documents to be added with key=document
   *          name value = string representation of json document.
   * @return staus of request true/false
   */
  public boolean addBulkDocument(String indexName, String indexType, Map<String, String> docs) {

    boolean bulkResult = false;
    BulkRequestBuilder bulkRequest = client.prepareBulk();

    if (ObjectUtils.allNotNull(docs) && !docs.isEmpty()) {
      docs.forEach((k, v) -> {
        if (StringUtils.isNotBlank(k) && StringUtils.isNotBlank(v)) {
          IndexRequest indexReq = new IndexRequest(indexName);
          indexReq.type(indexType);
          indexReq.id(k);
          indexReq.source(v);
          bulkRequest.add(indexReq);
        }
      });
    }

    if (ObjectUtils.allNotNull(bulkRequest)) {
      BulkResponse bulkResponse = bulkRequest.get();
      if (!bulkResponse.hasFailures()) {
        bulkResult = true;
      }
    }
    return bulkResult;
  }
  
 /**
  * Returns a list of all documents from a specified index.
  * @param indexName
  * @param indexType
  * @return
  */
	public List<String> getAllDocuments(String indexName, String indexType) {
		List<String> results = new ArrayList<String>();
		try {
			if (ObjectUtils.allNotNull(client)) {

				SearchRequestBuilder requestBuilder = client.prepareSearch(indexName).setTypes(indexType)
						.setQuery(QueryBuilders.matchAllQuery());

				SearchHitIterator hitIterator = new SearchHitIterator(requestBuilder);
				while (hitIterator.hasNext()) {
					SearchHit hit = hitIterator.next();
                    if(ObjectUtils.anyNotNull(hit)){
                    	if(ObjectUtils.anyNotNull(hit.getSourceAsString())){
                    		results.add(hit.getSourceAsString());
                    	}
                    }
				}
			}
		} catch (Exception e) {
			LOGGER.debug(e);
		} finally {
		}
       return results;
	}

	/**
	 * Handles search results returning more than 10000 results
	 * 
	 */
	public List<String> searchLargeResults(String indexName, String indexType, Map<String, Object> queryParams) {
		List<String> results = new ArrayList<String>();
		if (ObjectUtils.allNotNull(client,indexName,indexType,queryParams)) {
		QueryBuilder searchQuery = matchQueryBuilder(queryParams);

		SearchResponse scrollResp = client.prepareSearch(indexName).setTypes(indexType)
		        .addSort(FieldSortBuilder.DOC_FIELD_NAME, SortOrder.ASC)
		        .setScroll(new TimeValue(60000))
		        .setQuery(searchQuery)
		        .setSize(1000).get(); 
		//max of 1000 hits will be returned for each scroll
		//Scroll until no hits are returned
		do {
			for (SearchHit hit : scrollResp.getHits().getHits()) {
				if (ObjectUtils.anyNotNull(hit)) {
					if (ObjectUtils.anyNotNull(hit.getSourceAsString())) {
						results.add(hit.getSourceAsString());
					}
				}
			}

		    scrollResp = client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(60000)).execute().actionGet();
		} while(scrollResp.getHits().getHits().length != 0); // Zero hits mark the end of the scroll and the while loop.
	  }	
		return results;
	}

  /**
   * Builds an elastic search query based on the search criteria. The query is a
   * bool query with nested should and term query for each queryParam key/value
   * pair.
   * 
   * @param queryParams
   *          map with key value pair search criteria.
   * @return QueryBuilder object constructed with the queryparams
   */
  public QueryBuilder termQueryBuilder(Map<String, Object> queryParams) {

    BoolQueryBuilder searchQuery = QueryBuilders.boolQuery();
    if (ObjectUtils.allNotNull(queryParams) && !queryParams.isEmpty()) {
      queryParams.forEach((k, v) -> {
        if (ObjectUtils.allNotNull(k,v)) {
          TermQueryBuilder termQuery = QueryBuilders.termQuery(k, v);
          searchQuery.must(termQuery);
        }
      });
    }
    return searchQuery;
  }
  
  /**
   * Builds an elastic search query based on the search criteria. The query is a
   * bool query with nested should and match query for each queryParam key/value
   * pair.
   * 
   * @param queryParams
   *          map with key value pair search criteria.
   * @return QueryBuilder object constructed with the queryparams
   */
  public QueryBuilder matchQueryBuilder(Map<String, Object> queryParams) {

    BoolQueryBuilder searchQuery = QueryBuilders.boolQuery();
    if (ObjectUtils.allNotNull(queryParams) && !queryParams.isEmpty()) {
      queryParams.forEach((k, v) -> {
        if (ObjectUtils.allNotNull(k,v)) {
          MatchQueryBuilder matchQuery = QueryBuilders.matchQuery(k, v);
          searchQuery.must(matchQuery);
        }
      });
    }
    return searchQuery;
  }
  
  /**
   * Initializes the bulk processors to process a bulk of request. This shall
   * flush the requests when either 1000 or every 60 sec. The size set for docs
   * is 1GB and number of concurrent requests are 1
   */
    public void intializeBulkProcessor() {
  
      try {
        if (client != null) {
           bulkProcessor = BulkProcessor.builder(client, new BulkProcessor.Listener() {
            @Override
            public void beforeBulk(long executionId, BulkRequest request) {
              //System.out.println("Executing new bulk composed of" + request.numberOfActions() + "actions");
            }
  
            @Override
            public void afterBulk(long executionId, BulkRequest request, BulkResponse response) {
             // System.out.println("Executed bulk composed of " + request.numberOfActions() + " actions");
            }
  
            @Override
            public void afterBulk(long executionId, BulkRequest request, Throwable failure) {
             // System.out.println("Error executing bulk " + failure);
           }
          }).setBulkActions(10000)
              .setBulkSize(new ByteSizeValue(1, ByteSizeUnit.GB))
              .setFlushInterval(TimeValue.timeValueSeconds(60))
              .setConcurrentRequests(1)
              .build();
        }
      } catch (Exception e) {
        e.printStackTrace();
     }
    }
    
    /**
     * Flushes  the request in bulk processor and closes the same.
     */
    public void flushBulkProcessor(){
      try{
       if (bulkProcessor != null) {
          bulkProcessor.flush();
        }
      }catch(Exception e){
        e.printStackTrace();
      }
     }
    
    
    /**
     * Flushes  the request in bulk processor and closes the same.
     */
    public void closeBulkProcessor(){
      try{
        if (bulkProcessor != null) {
          bulkProcessor.close();
        }
      }catch(Exception e){
        e.printStackTrace();
      }
     }

	/**
	 * Gets the bulk processor
	 */
	public BulkProcessor getBulkProcessor() {
		return bulkProcessor;
	}
}
