package com.esri.elastic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.elasticsearch.common.logging.Loggers;

// test coimmit master
// test comment 
/**
 * Util methods to manage elastic node. methods to execute start and stop of elastic search process.
 * @author anik7857
 *
 */
public class ElasticUtil {
	
	private static Logger LOGGER = Loggers.getLogger(ElasticUtil.class);
	
	/**
	 * Executes the given command line and returns status of operation.
	 *
	 * @param cmd
	 *            The command to be executed
	 * @param envp
	 *            System environment variables.
	 * @param workDir the work directory  
	 * @param waitProcess to wait for command execution         
	 * @return - the integer exit status code of the command
	 */
	public static int execCommand(String[] cmd, String[] envp, String workDir, boolean waitProcess) {
		try {
			Runtime runTime = Runtime.getRuntime();
			Process p = runTime.exec(cmd, envp, new File(workDir));
			// To prevent hang if much output, read and discard any output.
			StreamReader errorReader = new StreamReader(p.getErrorStream(), "ERROR");
			StreamReader outputReader = new StreamReader(p.getInputStream(), "OUTPUT");
			errorReader.start();
			outputReader.start();
			if (waitProcess) {
				int exitVal = p.waitFor();

				if (errorReader.getLines().size() > 0) {
					for (String line : errorReader.getLines())
						System.out.println(line);
				}

				return exitVal;
			} else {
				return 0;
			}
		} catch (Exception e) {
			LOGGER.debug(e);
			return -1;
		}
	}
	
	/**
	 * Kills an elastic instance from pid file
	 * @param runtimeDir the directory path containing the pid file.
	 * @throws Exception throws IOException
	 */
	public static void killElasticsearchFromPIDFile(String runtimeDir) throws Exception {

		String pidFile = runtimeDir + File.separator + "pids" + File.separator + "elastic.pid";
		if (!(new File(pidFile)).exists())
			return;

		String strPID = readFileAsString(pidFile).trim();
		Integer pid = null;
		if (strPID != null) {
			try {
				pid = Integer.valueOf(strPID);
			} catch (Exception e) {
				LOGGER.debug(e);
				return;
			}
		}
		kill(pid);
	}
		
	/**
	 * Execute command to start an elastic instance
	 * @param runtimeDir the elastic search bin directory root
	 * @param command the command to be executed
	 * @param waitProcess to wait for process.
	 * @return status of execution true or false
	 */
	public static int executeElasticCommand(String runtimeDir, String command, boolean waitProcess) {
		int status = 0;
		String cmdLocation = runtimeDir + File.separator + "elasticsearch" + File.separator + "bin";
		if (System.getProperty("os.name").toLowerCase().contains("linux")) {
			try {
				String envp[] = null;
				envp = new String[] { "JAVA_HOME=" + runtimeDir + File.separator + "jre" };
				status = execCommand(new String[] { "bash", "-c", command, }, envp, cmdLocation, waitProcess);
			} catch (Exception ex) {
			}
		} else if (System.getProperty("os.name").toLowerCase().contains("windows")) {
			try {
				LinkedList<String> envs = new LinkedList<String>();
				// if (bManageHeapSize && heapSize != null) {
				envs.add("JAVA_HOME=" + runtimeDir + File.separator + "jre");
				status = execCommand(new String[] { "cmd", "/C", command, }, envs.toArray(new String[envs.size()]),
						cmdLocation, waitProcess);
			} catch (Exception ex) {
				LOGGER.debug(ex);
			}
		}
		if (status != 0) {
		}
		return status;
	}
	
	private static  String readFileAsString(String pathToFile) throws Exception {
		FileInputStream fIn = null;
		BufferedReader bufferedReader = null;
		try {
			/* FileReader */
			fIn = new FileInputStream(pathToFile);

			/* Open a buffered reader */
			bufferedReader = new BufferedReader(new InputStreamReader(fIn, Charset.forName("UTF-8")));
			StringBuilder strBuilder = new StringBuilder();

			/* Read into buffer */
			char[] buffer = new char[1024];
			int read = 0;
			while ((read = bufferedReader.read(buffer, 0, 1024)) != -1) {
				strBuilder.append(buffer, 0, read);
			}

			/* done */
			return strBuilder.toString();
		} finally {
			try {

				/* Close the opened readers */
				fIn.close();
				bufferedReader.close();
			} catch (Exception ignoreEx) {
				LOGGER.debug(ignoreEx);
			}
		}
	}

	private static void kill(int pid) {
		try {
			if (System.getProperty("os.name").toLowerCase().contains("windows")) {
				String cmd = "taskkill /F /PID " + pid;
				Runtime.getRuntime().exec(cmd);
			}
		} catch (Exception e) {
			LOGGER.debug(e);
		}
	}
	
	
	private static class StreamReader extends Thread {
		InputStream is;
		String type;

		private List<String> lines = new ArrayList<String>();

		StreamReader(InputStream is, String type) {
			this.is = is;
			this.type = type;
		}

		public List<String> getLines() {
			return lines;
		}

		public void run() {
			BufferedReader br = null;
			try {
				InputStreamReader isr = new InputStreamReader(is);
				br = new BufferedReader(isr);
				String line = null;
				while ((line = br.readLine()) != null)
					lines.add(line);
				;
			} catch (IOException ioe) {} 
			finally {
				try {
					if (br != null)
						br.close();
				} catch (IOException ioe) {
				}
			}
		}
	}

}
